const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const app = express();
require('express-ws')(app);


app.use(cors());
app.use(express.json());

const users = require('./app/users');
const chat = require('./app/chat');


mongoose.connect(config.db);

const db = mongoose.connection;

db.once('open', () => {
    console.log('Mongoose loaded!');

    app.use('/users', users());
    app.use('/chat', chat());

    const port = 8000;

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});

