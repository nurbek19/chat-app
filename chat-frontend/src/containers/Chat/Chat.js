import React, {Component} from 'react';
import {connect} from 'react-redux';

import './Chat.css';
import {
    deleteMessage,
    fetchAllMessages,
    fetchLoggedInUser,
    fetchLoggedInUsers,
    fetchNewMessage
} from "../../store/actions/chat";

class Chat extends Component {
    state = {
        messageText: '',
    };

    componentWillUnmount() {
        this.websocket.close();
    }

    start = token => {
        this.websocket = new WebSocket(`ws://localhost:8000/chat?token=${token}`);

        this.websocket.onmessage = (message) => {

            const decodedMessage = JSON.parse(message.data);

            switch (decodedMessage.type) {
                case 'NEW_MESSAGE':
                    this.props.fetchNewMessage(decodedMessage.message);
                    break;
                case 'LOGGED_IN_USERS':
                    this.props.fetchAllUsers(decodedMessage.users);
                    break;
                case 'LAST_MESSAGES':
                    this.props.fetchAllMessages(decodedMessage.messages);
                    break;
                case 'USER_LOGGED_IN':
                    this.props.fetchUser(decodedMessage.user);
                    break;
                case 'DELETE_MESSAGE':
                    this.props.deleteMessage(decodedMessage.id);
                    break;
                default:
                    console.log('Unknown message: ', decodedMessage);
            }
        };

        this.websocket.onopen = () => {
          console.log('Connection established');
        };

        this.websocket.onclose = () => {
            setTimeout(() => {
                this.start(token);
                console.log('Problems with your connection!');
            }, 5000);
        };
    };

    componentDidMount() {
        this.start(this.props.user.token);
    }

    changeMessageTextHandler = event => {
        this.setState({messageText: event.target.value});
    };

    sendMessageHandler = event => {
        event.preventDefault();

        const message = JSON.stringify({
            type: 'CREATE_MESSAGE',
            text: this.state.messageText
        });

        this.websocket.send(message);
        this.setState({messageText: ''});
    };

    removeMessage = (event, id) => {
      event.preventDefault();

      this.websocket.send(JSON.stringify({
          type: 'DELETE_MESSAGE',
          id
      }));
    };

    render() {
        return (
            <div className="chat-container">
                <div className="users-list">
                    <h2>Users</h2>
                    {this.props.users ? this.props.users.map((user, i) => (
                        <p key={i}><b>{user.username}</b></p>
                    )) : 'Loading...'}
                </div>

                <div className="chat-block">
                    <h2>Messages</h2>
                    {this.props.messages ? this.props.messages.map(message => (
                        <p key={message._id}>
                            <b>{message.user.username}: </b>
                            {message.text}
                            <a href="#"
                               onClick={(event) => this.removeMessage(event, message._id)}
                               style={{marginLeft: '10px'}}
                            >Delete</a>
                        </p>
                    )) : 'Loading...'}

                    <form onSubmit={this.sendMessageHandler}>
                        <input type="text" required
                               value={this.state.messageText}
                               onChange={this.changeMessageTextHandler}/>
                        <button type="submit">Send</button>
                    </form>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    messages: state.chat.messages,
    users: state.chat.users
});

const mapDispatchToProps = dispatch => ({
    fetchNewMessage: message => dispatch(fetchNewMessage(message)),
    fetchAllMessages: messages => dispatch(fetchAllMessages(messages)),
    fetchAllUsers: users => dispatch(fetchLoggedInUsers(users)),
    fetchUser: user => dispatch(fetchLoggedInUser(user)),
    deleteMessage: id => dispatch(deleteMessage(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(Chat);