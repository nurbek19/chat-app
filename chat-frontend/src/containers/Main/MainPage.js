import React from 'react';
import {PageHeader} from "react-bootstrap";

const MainPage = () => (
  <PageHeader style={{padding: '200px 0'}}>Для того чтобы начать переписываться, пожалуйста залогиньтесь!</PageHeader>
);

export default MainPage;