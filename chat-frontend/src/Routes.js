import React from 'react';
import {Route, Switch} from "react-router-dom";

import Login from "./containers/Login/Login";
import Register from "./containers/Register/Register";
import Chat from "./containers/Chat/Chat";
import MainPage from "./containers/Main/MainPage";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={MainPage}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/chat" exact component={Chat}/>
        </Switch>
    );
};

export default Routes;