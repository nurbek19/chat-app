import {
    DELETE_MESSAGE,
    FETCH_ALL_MESSAGES,
    FETCH_LOGGED_IN_USER,
    FETCH_LOGGED_IN_USERS,
    FETCH_NEW_MESSAGE
} from "./actionTypes";

export const fetchNewMessage = message => ({
   type: FETCH_NEW_MESSAGE, message
});

export const fetchAllMessages = messages => ({
   type: FETCH_ALL_MESSAGES, messages
});

export const fetchLoggedInUsers = users => ({
   type: FETCH_LOGGED_IN_USERS, users
});

export const fetchLoggedInUser = user => ({
    type: FETCH_LOGGED_IN_USER, user
});

export const deleteMessage = id => ({
   type: DELETE_MESSAGE, id
});